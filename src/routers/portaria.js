const express = require('express');
const Portaria = require('../models/portaria');

const router = new express.Router()


router.post('/portarias', async (req, res) => {
    const portaria = new Portaria(req.body);

    try {
        await portaria.save();
        res.status(201).send(portaria);
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    };
});

router.get('/portarias', async(req, res) => {
    try {
        const portarias = await Portaria.find({});
        res.status(200).send(portarias);
    } catch (error) {
        res.status(400).send(error);
    }
});

router.get('/portarias', async (req, res) => {
    const portaria = new Portaria(req.body);

    try {
        await portaria.save();
        res.status(201).send(portaria);
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    };
});

router.post('/portarias/alter-password', async (req, res) => {
    try {
        const portaria = await Portaria.findByCredentials(req.body.id, req.body.older_password);
        portaria.password = req.body.new_password
        await portaria.save();
        res.status(201).send(portaria);
    } catch (error) {
        console.log(error);
        res.status(400).send({errorCode: error});
    };
});

router.post('/portarias/open', async (req, res) => {
    try {
        const portaria = await Portaria.findByCredentials(req.body.id, req.body.password);
        res.send({portaria});
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    };
});

module.exports = router;