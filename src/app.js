const express = require('express');
const portariaRouter = require('./routers/portaria');
require('./db/mongoose');
const dotenv = require('dotenv');

const app = express();

app.use(express.json());
app.use(portariaRouter);
dotenv.config();

app.listen(process.env.PORT, () => {
    console.log('server listening on port ' + process.env.PORT);
});