const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
var uuid = require('node-uuid');

const portariaSchema = new mongoose.Schema({
    _id: {type: String, default: function genUUID() {
        uuid.v1();
    }},
    password: {
        type: String,
        required: true,
        Length: 4,
        trim: true,
    },
}, {
    timestamps: true
});

portariaSchema.statics.findByCredentials = async (id, password) => {
    const portaria = await Portaria.findOne({ _id: id });
    if (!portaria) {
        throw new Error('Unable');
    };
    console.log(portaria);
    const isMatch = await bcrypt.compare(password, portaria.password);
    console.log(isMatch);
    if(!isMatch) {
        throw new Error('Unable');
    };

    return portaria;
};

portariaSchema.pre('save', async function(next) {
    const portaria = this;
    if (portaria.isModified('password')) {
        portaria.password = await bcrypt.hash(portaria.password, 4);
    };

    next();
});


const Portaria = mongoose.model('Portaria', portariaSchema);

module.exports = Portaria;
